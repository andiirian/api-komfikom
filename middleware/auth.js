module.exports ={

    checkLoginAdmin : (req, res, next) =>{
        if (req.session.logged_in != true && req.session.role != 1){
            res.redirect('/users/login')
        }
        else next()
    }
}