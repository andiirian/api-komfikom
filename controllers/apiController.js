const {knex} = require('../database/db')
const moment = require('moment')
const redis = require('redis')
const client = redis.createClient()
client.on("error", err => console.log(err))
module.exports = {
    // index : async (req, res, next) =>{
    //     let data = await Promise.all([
    //         knex('tbl_kat_dokumentasi').select(),
    //         knex('tbl_blogs').select().limit(3),
    //         knex('tbl_pengurus').select(),
    //         knex('tbl_visi_misi').select(),
    //         knex('tbl_sejarah').select()
    //     ])
    //     for (let i = 0; i < data[0].length; i++){
    //         let dok = await knex('tbl_dokumentasi')
    //             .select()
    //             .where({id_kat_dokumentasi : data[0][i].id})
    //         data[0][i].dokumentasi = dok
    //
    //     }
    //
    //     res.send({
    //         data_dokumentasi : data[0],
    //         data_blogs : data[1],
    //         data_pengurus : data[2],
    //         data_visi : data[3][0],
    //         data_sejarah : data[4][0]
    //
    //     })
    //
    // },
    getDataVisi : async (req, res, next) =>{
        try{
            let data = await knex('tbl_visi_misi')
            client.set('api:tbl_visi_misi',JSON.stringify(data), redis.print)
            res.send(data)
            console.log('api')
        }catch (e) {
            console.log(e)
        }
    },
    getDataArchive : async (req, res, next) =>{
        try {
            let data = await knex('tbl_blogs')
                .select(knex.raw(`count('id') as total, DATE_FORMAT(tgl, '%M %Y') as tgl`))
                .groupByRaw(`DATE_FORMAT(tgl, '%M %Y')`)
            client.set('api:archive', JSON.stringify(data), redis.print)
            res.send(data)
            console.log('api')
        }catch (e) {
            console.log(e)
        }
    },
    getDataDetailBlog : async (req, res, next) =>{
        try{
            let data = await knex('tbl_blogs')
                .where({id : req.params.id})
            client.set(`api:detailBlog:${req.params.id}`, JSON.stringify(data[0]))
            res.send(data[0])
            console.log('api')
        }catch (e) {
            console.log(e)
        }

    },
    getDataBlogs : async (req, res, next) =>{
        let tgl = moment().format("MMMM YYYY")
        let halaman = req.query.halaman,
            mulai = 0,
            page = req.query.page
        let op = "<="
        if (req.query.archive != undefined && req.query.archive != 0 ){
            tgl = req.query.archive
            op = "="
        }
        let total = await knex('tbl_blogs').count(`id as total`)
            .whereRaw(`DATE_FORMAT(tgl, '%M %Y') ${op} '${tgl}'`)

        if (page > 1){
            mulai = (page * halaman) - halaman
        }

            knex('tbl_blogs')
                .whereRaw(`DATE_FORMAT(tgl, '%M %Y') ${op} '${tgl}' LIMIT ${mulai}, ${halaman}`)
                .then(result =>{
                    res.send({
                        data : result,
                        total : total[0].total,
                        per_page : halaman
                    })
                })

    },
    getDataAllPengurus : async (req, res, next) =>{

        let halaman = req.query.halaman,
            page = req.query.page,
            mulai = 0
        let periode = "(SELECT id FROM tbl_periode ORDER BY id DESC LIMIT 1)"
        if (req.query.periode > 0){
            periode = req.query.periode
        }
        if (page > 1) {
            mulai = (page * halaman) - halaman
        }
        let total =  await knex('tbl_pengurus')
            .count('id as total')
            .whereRaw(`id_periode = ${periode}`)

       knex('tbl_pengurus')
           .whereRaw(`id_periode = ${periode} LIMIT ${mulai}, ${halaman}`)
           .then(result =>{
               res.send({
                   data : result,
                   total : total[0].total,
                   per_page : halaman
               })
           })

    },
    getDataPeriode : async (req, res, next) =>{
        try{
            let data = await knex('tbl_periode')
            data.forEach( val => {
                client.rpush('api:periode', JSON.stringify(val))
            })
            res.send(data);
            console.log('api')
        }catch (e) {
            console.log(e)
        }

    },
    getRedis : async (req, res, next) => {
        try {
            let dataBlogs = await knex('tbl_blogs').select()
            client.set('user:tbl_blogs',JSON.stringify(dataBlogs), redis.print)
            res.send(dataBlogs)
        }catch (e) {
            console.log(e)
        }

    }


}