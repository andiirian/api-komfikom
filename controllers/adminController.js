const {knex, koneksi} = require('../database/db')
const passwordHash = require('password-hash')
var error = null
const NodeTable = require('nodetable')
module.exports = {
    getLogin: (req, res, next) => {
        res.render('login/login', {
            error: error,
            url : 'users'
        })
        error = null
    },
    postLogin: (req, res, next) => {
        let data = {
            username: req.body.username,
            password: req.body.password
        }
        knex('tbl_admin').where({username : req.body.username})
            .then(result => {
                if (result.length != 0) {
                    if (passwordHash.verify(data.password, result[0].password)) {
                        req.session.role = 1
                        req.session.logged_in = true
                        res.redirect('/users')
                    } else {
                        error = 1
                        res.redirect('/users/login')
                    }
                } else {
                    error = 2
                    res.redirect('/users/login')
                }
            })
    },
    getIndex : async (req,res, next) =>{
        let data = await Promise.all([
            knex('tbl_admin').count('id as total'),
            knex('tbl_periode').count('id as total'),
            knex('tbl_pengurus').count('id as total')
                .whereRaw(`id_periode = (SELECT id FROM tbl_periode ORDER BY id DESC LIMIT 1)`)
        ])
        res.render('admin/index',{
            dataTotalAdmin : data[0][0].total,
            dataTotalPeriode : data[1][0].total,
            dataTotalPengurus : data[2][0].total
        })

    },
    getTableAdmin : (req, res, next) =>{
        res.render('admin/table-admin')
    },
    postFormAdmin : (req, res, next) =>{
        let data = {
            nama : req.body.nama,
            email : req.body.email,
            notlp : req.body.notlp,
            username : req.body.username,
            password : passwordHash.generate(req.body.password)
        }
        knex('tbl_admin').insert(data)
            .then(result =>{ return result ? res.send(true) : res.send(false)})
    },
    getDataAdmin : (req, res, next) =>{
        let requestQuery = req.query,
            columns = [{db : "nama", dt : 0},{db :"email", dt : 1},
                {db :"notlp", dt : 2},{db :"username", dt : 3},
                {db :"id", dt : 4}
            ],
            table = "tbl_admin",
            primaryKey = "id",
            nodeTable = new NodeTable(requestQuery,koneksi,table,primaryKey,columns)
        nodeTable.output((err, result) =>{
            return err ? console.log(err) : res.send(result)
        })
    },
    getFormVisi : (req, res, next) =>{
        knex('tbl_visi_misi').select()
            .then(result =>{
                res.render('admin/form-visi',{
                    data : result[0],
                    error : error
                })
                error = null
            })
    },
    postFormVisi : (req, res, next) =>{
        let data = {
            visi : req.body.visi,
            misi : req.body.misi,
            tujuan : req.body.tujuan
        }
        knex('tbl_visi_misi').update(data).where({id : 1})
            .then(result =>{
                if (result){
                    error = 1
                    res.redirect(req.originalUrl)
                }else{
                    error = 2
                    res.redirect(req.originalUrl)
                }
            })
    },
    getTableBlog : (req, res, next) =>{
        res.render('admin/table-blog')
    },
    getDataBlog : (req, res, next) =>{
        let requestQuery = req.query,
            columns = [{db : "foto", dt : 0}, {db : "judul", dt: 1},{db : "tanggal", dt : 2},{db : "id", dt : 3}],
            primaryKey = "id",
            table = `SELECT *, DATE_FORMAT(tgl, \"%d %M %Y\") as tanggal FROM tbl_blogs`
        const nodeTable = new NodeTable(requestQuery,koneksi,table,primaryKey,columns)

        nodeTable.output((err, result) =>{
            if (err){
                console.log(err)
            }else{
                res.send(result)
            }
        })

    },
    getEditBlog : (req, res, next) =>{
        knex('tbl_blogs').select('*', knex.raw(`DATE_FORMAT(tgl, "%Y-%m-%d") as tanggal`))
            .where({id : req.params.id})
            .then(result =>{
                res.render('admin/form-edit-blog',{
                    data : result[0],
                    error : error
                })
                error = null
            })
    },
    postEditBlog : (req, res, next) =>{
        let data
        console.log(req.file)
        if (req.file === undefined){
            data = {
                judul : req.body.judul,
                desc_blog : req.body.desc_blog,
                tgl : req.body.tgl
            }
        }else{
            data = {
                judul : req.body.judul,
                desc_blog : req.body.desc_blog,
                tgl : req.body.tgl,
                foto : `http://${req.headers.host}/images/foto-blog/${req.file.filename}`
            }
        }
        knex('tbl_blogs').update(data).where({id : req.params.id})
            .then(result =>{
                if (result){
                    error = 1
                    res.redirect(req.originalUrl)
                }else{
                    error = 2
                    res.redirect(req.originalUrl)
                }
            })
    },
    postDelBlog : (req, res, next) =>{
        knex('tbl_blogs').where({id : req.body.id}).del()
            .then(result =>{
                return result ? res.send(true) : res.send(false)
            })
    },
    getFormBlog : (req, res, next) =>{
        res.render('admin/form-blog', {
            error : error
        })
        error = null
    },
    postFormBlog : (req, res, next) =>{
        let data = {
            judul : req.body.judul,
            desc_blog : req.body.desc_blog,
            tgl : req.body.tgl,
            foto : `http://${req.headers.host}/images/foto-blog/${req.file.filename}`
        }
        knex('tbl_blogs').insert(data)
            .then(result =>{
               error = 1
                res.redirect(req.originalUrl)
            })
            .catch(err =>{
                error = 2
                res.redirect(req.originalUrl)
            })
    },
    getFormSejarah : (req, res, next) =>{
        knex('tbl_sejarah')
            .then(result =>{
                res.render('admin/form-sejarah',{
                    data : result[0],
                    error : error
                })
                error = null
            })
    },
    postFormSejarah : (req, res, next) =>{
        knex('tbl_sejarah').update({
            judul : req.body.judul,
            desc_sejarah : req.body.desc_sejarah
        }).where({id : 1})
            .then(result =>{
                if (result){
                    error = 1
                    res.redirect(req.originalUrl)
                }else{
                    error = 2
                    res.redirect(req.originalUrl)
                }
            })
            .catch(err =>{
                console.log(err)
                error = 2
                res.redirect(req.originalUrl)
            })

    },
    getTableKatDokumentasi : (req, res, next) =>{
        res.render('admin/table-kat-dokumentasi')
    },
    getDataKatDokumentasi : (req, res, next) =>{
        let requestQuery = req.query
        let columns = [{db : "kategori", dt : 0},{db : "id", dt : 1}]
        let primaryKey = "id"
        let table = "tbl_kat_dokumentasi"

        let nodeTable = new NodeTable(requestQuery,koneksi,table,primaryKey,columns)

        nodeTable.output((err, data) =>{

            if (err){
                console.log(err)
            }else{
                res.send(data)
            }
        })
    },
    postFormKatDokumentasi : (req, res, next) =>{
        knex('tbl_kat_dokumentasi').insert({kategori : req.body.kategori})
            .then(result =>{
                return result ? res.send(true) : res.send(false)
            })
    },
    datakatKategori : (req, res, next) =>{
        knex('tbl_kat_dokumentasi').select().where({id : req.body.id})
            .then(result =>{
                console.log(result)
                res.send(result)
            })

    },
    postEditKatDokumentasi : (req,res, next) =>{
        knex('tbl_kat_dokumentasi').update({
            kategori : req.body.kategori
        })
            .where({id : req.body.id})
            .then(result =>{

                return result ? res.send(true) : res.send(false)
            })
    },
    postDelKatDokumentasi : (req, res, next) =>{
        knex('tbl_kat_dokumentasi')
            .where({id : req.body.id})
            .del()
            .then(result =>{
                console.log(result)
                return result ? res.send(true) : res.send(false)
            })
    },
    getTableDokumentasi : (req, res, next) =>{
        res.render('admin/table-dokumentasi')
    },
    getTableDetailDokumentasi : (req, res, next) =>{
      res.render('admin/table-detail-dokumentasi',{
          id : req.params.id
      })
    },
    getDataDetailDokumentasi : (req, res, next) =>{
      let requestQuery = req.query,
          colums = [{db : "foto",dt : 0},{db : "judul",dt : 1},{db : "id",dt : 2}],
          query = knex('tbl_dokumentasi').select().where({id_kat_dokumentasi : requestQuery.id_kat}).toString(),
          primaryKey = "id",
          nodeTable = new NodeTable(requestQuery, koneksi, query, primaryKey,colums)
        nodeTable.output((err, data) =>{
            if (err){
                console.log(err)
            }else {
                res.send(data)
            }
        })

    },
    postFormDokumentasi : (req, res, next) =>{
        let data ={
            id_kat_dokumentasi : req.params.id,
            judul : req.body.judul,
            foto : `http://localhost:8080/images/foto-dokumentasi/${req.file.filename}`
        }
        knex('tbl_dokumentasi').insert(data)
            .then(result =>{

                    res.redirect(req.originalUrl)

            })
    },
    getTablePeriode : (req, res, next) =>{
        res.render('admin/table-periode')
    },
    getDataPeriode : (req, res, next) =>{
        let requestQuery = req.query,
            columns = [{db : "periode", dt : 0}, {db : "id", dt : 1}],
            table = "tbl_periode",
            primaryKey = 'id'
            nodeTable = new NodeTable(requestQuery, koneksi,table,primaryKey,columns)
        nodeTable.output((err, data)=>{
            if (err){
                console.log(err)
            }else{
                res.send(data)
            }
        })
    },
    getPeriode : (req, res, next) =>{
        knex('tbl_periode').where({id : req.body.id})
            .then(result =>{
                return result ? res.send(result) : res.send(false)
            })
    },
    postEditPeriode : (req, res, next) =>{
        knex('tbl_periode').update({periode : req.body.periode}).where({id : req.body.id})
            .then(result =>{
                return result ? res.send(true) : res.send(false)
            })
    },
    postDelperiode : (req, res, next) =>{
        knex('tbl_periode').where({id : req.body.id}).del()
            .then(result =>{
                return result ? res.send(true) : res.send(false)
            })
    },
    postFormPeriode : (req, res, next) =>{
        knex('tbl_periode').insert({periode : req.body.periode})
            .then(result =>{
                return result ? res.send(true) : res.send(false)

            })
    },
    getTableJabatan : (req, res, next) =>{
        res.render('admin/table-jabatan')
    },
    getDataJabatan : (req, res, next) =>{
        let requestQuery = req.query,
            columns = [{db : "nama_jabatan", dt : 0}, {db : "id", dt : 1}],
            table = "tbl_jabatan",
            primaryKey = 'id'
        nodeTable = new NodeTable(requestQuery, koneksi,table,primaryKey,columns)
        nodeTable.output((err, data)=>{
            if (err){
                console.log(err)
            }else{
                res.send(data)
            }
        })
    },
    postFormJabatan : (req, res, next) =>{
        knex('tbl_jabatan').insert({nama_jabatan : req.body.nama_jabatan})
            .then(result =>{
                return result ? res.send(true) : res.send(false)
            })
    },
    getTablePeriodePengurus : (req, res, next) =>{
        res.render('admin/tbl-periode-pengurus')
    },
    getTablePengurus : (req, res, next) =>{
        res.render('admin/table-pengurus',{
            id_periode : req.params.id_periode,
        })
    },
    getDataPengurus : (req, res, next) =>{
        let requestQuery = req.query,
            columns = [{db : "nama", dt : 0}, {db : "nama_jabatan", dt : 1},{db : "id", dt : 2}],
            query = knex('tbl_pengurus')
                .where({
                    id_periode : requestQuery.id_periode,
                })
                .select()
                .toString(),
            primaryKey = "id",
            nodeTable = new NodeTable(requestQuery,koneksi,query,primaryKey,columns)
        nodeTable.output((err, data)=>{
            if (err){
                console.log(err)
            }else {
                res.send(data)
            }
        })
    },
    postFormPengurus : (req, res, next) =>{
        let data = {
            id_periode : req.params.id_periode,
            id_jabatan : req.params.id_jabatan,
            nama : req.body.nama,
            nama_jabatan:  req.body.nama_jabatan,
            kutipan : req.body.kutipan,
            twitter : req.body.twitter,
            google_plus : req.body.google_plus,
            instagram : req.body.instagram,
            facebook : req.body.facebook,
            foto : `http://localhost:8080/images/foto-pengurus/${req.file.filename}`
        }
        knex('tbl_pengurus')
            .insert(data)
            .then(result =>{
                res.redirect(req.originalUrl)
            })
    }


}