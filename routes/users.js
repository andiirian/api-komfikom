var express = require('express');
var router = express.Router();
let multer = require('multer')
let path = require('path')
const {checkLoginAdmin} = require('../middleware/auth')
const adminController = require('../controllers/adminController')
const STORAGE1 = multer.diskStorage({
    destination: path.join(__dirname + './../public/images/foto-blog/'),
    filename: function (req, file, cb) {
        cb(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname));
    }
});
const UPLOADPROFILE = multer({
    storage: STORAGE1
});
const STORAGE2 = multer.diskStorage({
    destination: path.join(__dirname + './../public/images/foto-dokumentasi/'),
    filename: function (req, file, cb) {
        cb(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname));
    }
});
const UPLOADDOKUMENTASI = multer({
    storage: STORAGE2
});
const STORAGE3 = multer.diskStorage({
    destination: path.join(__dirname + './../public/images/foto-pengurus/'),
    filename: function (req, file, cb) {
        cb(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname));
    }
});
const UPLOADPENGURUS = multer({
    storage: STORAGE3
});
/* GET users listing. */
router.get('/login', adminController.getLogin)
router.post('/login', adminController.postLogin)
router.get('/',checkLoginAdmin, adminController.getIndex);
router.get('/form-visi',checkLoginAdmin, adminController.getFormVisi)
router.post('/form-visi', checkLoginAdmin,adminController.postFormVisi)

router.get('/form-blog', checkLoginAdmin,adminController.getFormBlog)
router.post('/form-blog',checkLoginAdmin, UPLOADPROFILE.single('foto-blog'), adminController.postFormBlog)
router.get('/table-blog',checkLoginAdmin, adminController.getTableBlog)
router.get('/data.blog', checkLoginAdmin,adminController.getDataBlog)
router.get('/edit-blog/:id',checkLoginAdmin, adminController.getEditBlog)
router.post('/edit-blog/:id', checkLoginAdmin,UPLOADPROFILE.single('foto-blog'),adminController.postEditBlog)
router.post('/del.blog', checkLoginAdmin,adminController.postDelBlog)
router.get('/form-sejarah', checkLoginAdmin,adminController.getFormSejarah)
router.post('/form-sejarah', checkLoginAdmin,adminController.postFormSejarah)

router.get('/table-kat-dokumentasi', checkLoginAdmin,adminController.getTableKatDokumentasi)
router.get('/data.kat-dokumentasi',checkLoginAdmin, adminController.getDataKatDokumentasi)
router.post('/insert.kat-dokumentasi',checkLoginAdmin, adminController.postFormKatDokumentasi)
router.post('/kat-dokumentasi',checkLoginAdmin, adminController.datakatKategori)
router.post('/edit.kat-dokumentasi',checkLoginAdmin, adminController.postEditKatDokumentasi)
router.post('/del.kat-dokumentasi',checkLoginAdmin, adminController.postDelKatDokumentasi)

router.get('/table-dokumentasi',checkLoginAdmin, adminController.getTableDokumentasi)
router.get('/table-detail-dokumentasi/:id',checkLoginAdmin, adminController.getTableDetailDokumentasi)
router.get('/data.detail-dokumentasi',checkLoginAdmin, adminController.getDataDetailDokumentasi)
router.post('/table-detail-dokumentasi/:id',checkLoginAdmin, UPLOADDOKUMENTASI.single('foto-dokumentasi'), adminController.postFormDokumentasi)

router.get('/table-periode',checkLoginAdmin, adminController.getTablePeriode)
router.get('/data.periode',checkLoginAdmin, adminController.getDataPeriode)
router.post('/insert.periode',checkLoginAdmin, adminController.postFormPeriode)
router.post('/periode',checkLoginAdmin, adminController.getPeriode)
router.post('/edit.periode',checkLoginAdmin,adminController.postEditPeriode)
router.post('/del.blog',checkLoginAdmin, adminController.postDelperiode)

router.get('/table-jabatan', checkLoginAdmin,adminController.getTableJabatan)
router.get('/data.jabatan', checkLoginAdmin,adminController.getDataJabatan)
router.post('/insert.jabatan',checkLoginAdmin, adminController.postFormJabatan)

router.get('/table-admin',checkLoginAdmin, adminController.getTableAdmin)
router.get('/data.admin', checkLoginAdmin,adminController.getDataAdmin)
router.post('/table-admin',checkLoginAdmin, adminController.postFormAdmin)


router.get('/table-pengurus',checkLoginAdmin, adminController.getTablePeriodePengurus)
router.get('/table-pengurus/:id_periode',checkLoginAdmin, adminController.getTablePengurus)
router.get('/data.pengurus', checkLoginAdmin,adminController.getDataPengurus)
router.post('/table-pengurus/:id_periode/:id_jabatan',checkLoginAdmin,UPLOADPENGURUS.single('foto-pengurus'), adminController.postFormPengurus)
module.exports = router;
