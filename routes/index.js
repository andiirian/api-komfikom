var express = require('express');
var router = express.Router();
const apiController = require('../controllers/apiController')
const redisCache = require('../cache/redisCache')
/* GET home page. */
router.get('/data.visi',redisCache.getCacheVisiMisi, apiController.getDataVisi);
router.get('/data.archive',redisCache.getCacheArchive, apiController.getDataArchive)
router.get('/data.detail-blog/:id',redisCache.getCacheDetailBlog, apiController.getDataDetailBlog)
router.get('/data.blogs', apiController.getDataBlogs)
router.get('/data.pengurus', apiController.getDataAllPengurus)
router.get('/data.periode',redisCache.getCachePeriode, apiController.getDataPeriode)
router.get('/redis/:id', apiController.getRedis)


module.exports = router;
