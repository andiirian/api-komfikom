const redis = require('redis')
const client = redis.createClient()

module.exports = {
    getCacheBlogs : (req, res, next) =>{
        client.get('api:tbl_blogs',(err, data)=>{
            return data !== null ? res.send(JSON.parse(data)) : next()
        })
    },
    getCacheVisiMisi : (req, res, next) =>{
        client.get('api:tbl_visi_misi', (err, data)=>{
            return data !== null ? res.send(JSON.parse(data)) : next()
        })
    },
    getCacheDetailBlog : (req, res, next) =>{
        client.get(`api:detailBlog:${req.params.id}`, (err, data)=>{
            return data !== null ? res.send(JSON.parse(data)) : next()
        })
    },
    getCachePeriode : (req, res, next) =>{
        client.lrange('api:periode', 0,-1, (err, data)=>{
            if (data.length !== 0){
                let array = []
                data.forEach(val => array.push(JSON.parse(val)))
                res.send(array)
            }else{
                next()
            }
        })
    },
    getCacheArchive : (req, res, next) =>{
        client.get('api:archive', (err, data) =>{
            return data !== null ? res.send(JSON.parse(data)) : next()
        })
    }
}

